# GIF-3101 Notes [![pipeline status](https://gitlab.com/samcarpentier/gif-3101-notes/badges/master/pipeline.svg)](https://gitlab.com/samcarpentier/gif-3101-notes/commits/master)

To download latest generated PDF, click [here](https://gitlab.com/samcarpentier/gif-3101-notes/-/jobs/artifacts/master/raw/notes_gif_3101.pdf?job=create_pdf_artifact).
