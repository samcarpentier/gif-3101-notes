# Listes et menus (15 mars)

## Interactions multitouchers

* Toucher
* Long toucher (hold)
* Toucher et déplacer (swipe)
* Long toucher et déplacer (hold + swipe)
* Double toucher (double tap)
* Double toucher et déplacer
* Pinch (open/close): souvent zoom

### Reconnaissance du toucher dans Android

* MotionEvent: mouvement sur l'écran
* GestureDetector: reconnaissance mvt prédéfini
    * ::OnGestureListener: onDown, onPress, onLongPress, onFling, onSingleTapUp, ...
    * ::OnDoubleTapListener: onDoubleTap, onDoubleTapConfirmed, ...
* Multitude d'event listeners...

* Les touchers sont les premiers reçus dans l'activité (dispatchTouchEvent)
    * Retourne vrai si l'évènement a été consommé
    * Transmission au window qui dispatch selon la hiérarchie des views
    * Transmission arrête si une view retourne vrai (consommé)
    * Si non réclamé: fonction onTouchEvent dans l'activity parente

### Ordre de focus

* Défaut: Android se déplace vers le widget le plus proche (de gauche à droite, de haut en bas)
* Dans le XML de la view:
    * `android.nextFocusRight="@+id/controleSuivantADroite"`
    * `android.nextFocusDown="@+id/controleSuivantEnBas"`

## Listes

* Présent partout dans le monde mobile
* Structure logique des données
* Difficultés:
    * Gestion mémoire
    * Ne pas mélanger les données

### Recyclage de vues

* Réutiliser des vues et mettre uniquement son contenu à jour
* Évite le ramassage par le GC
* Avantages:
    * Diminue quantité de mémoire à allouer
    * Moins d'objets à allouer (plus rapide et fluide)
* Inconvénients:
    * Plus complexe à programmer
    * Memory leaks si erreurs de programmation

### Listes dans Android

* Data --> Adapter --> (AdapterView) -...-> Views (multiples)

#### Adapter

* Objet abstrait qui crée la bonne view selon le contenu
* Lien entre data et AdapterView
* BaseAdapter, ArrayAdapter, CursorAdapter, ListAdapter, ...
* Fonctions importantes:
    * `getView(int pos, View v, ViewGroup parent)`: Retourne la view à présenter
    * `isEmpty()`: Est-ce que l'adapter est vide? Utile pour afficher message du genre "Aucun contact"
    * `getItem(int pos)`: Retourne l'objet de data à la position spécifiée
    * `notifyDataSetChanged()`: Invalide les views et force à redessiner (définie dans BaseAdapter)

#### AdapterView

* Objet abstrait qui permet d'afficher et de gérer des views à partir d'un adapter
* ListView, GridView, Spinner, ExpandableListView
* Fonctions importantes:
    * `setOnItemClickListener(Listener l)`, `onItemClick (AdapterView<?> parent, View v, int pos, long id)`: Détection du tap sur une cellule
    * `setAdapter(T adapter)`
    * `setEmptyView(View v)`: View à afficher si aucun contenu
    * `getPositionForView(View v)`: Retourne la position de la view spécifiée
* Autres fonctions spécifiques à chaque sous-type dans les notes de cours (p.24)

#### ViewHolder

* Concept permettant d'éviter d'appeler `findById()` plusieurs fois
* Caching des IDs des views d'une cellule pour améliorer la performance
* Juste un gros DTO qui store les vues au lieu de les retrieve à chaque fois

#### RecyclerView

* Widget plus avancé et plus flexible qu'un ListView
* Uniquement dispo dans SupportLib V7
* Réutilisation de cellules
    * ListView: pour faire même chose, c'est un élément de programmation qu'il faut ajouter (ViewHolder)
* Liste d'éléments découpée en conteneurs
* Animation des éléments de la liste possible
* Avantages:
    * Offre différents layouts (présentation horizontale ou verticale)
    * Gestion plus facile de différents types de cellules
* Inconvénients:
    * Plus difficile à implémenter qu'un ListView
