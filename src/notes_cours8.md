# Services Web (22 mars)

## Modèle OSI (Cours réseaux)

* Bits:
    * Couche physique
* Trame:
    * Couche liaison
* Paquet (datagrame):
    * Couche réseau
* Segments:
    * Couche transport
* Données:
    * Couche session
    * Couche présentation
    * Couche application

## Quelques définitions

* RFC:
    * Request For Comment
    * Document formel tweaké par plusieurs parties intéressées
    * Une fois qu'il est final, aucune modification possible (sauf amandement par un autre RFC)
    * Standards internet sont souvent des RFCs

* IETF:
    * Internet Engineering Task Force
    * Groupe participant à l'élaboration des standards internets

## HTTP

* HyperText Transfer Protocol
* Modèle Request-Response dans une topologie Client-Serveur
* Codes de status pour faciliter la communication
* Méthode pour chaque type de requête
* Texte clair, sauf HTTPS (facile à intercepter)
* Port 80 par défaut (8080, 8008, 8000 souvent utilisés)

## HTTPS

* HyperText Transfer Protocol Secure
* HTTP + Couche SSL/TLS
* Port 443
* Utilisation et validation de certificats (Certificate Authority (CA))
* Basé sur certs locaux sur nos machines

## Méthodes HTTP

* GET:
    * Query params: http://host.com/api/endpoint?a=value1&b=value2
    * Conservée dans l'historique du browser, possibilité de mettre en favoris
    * **Pas d'info sensible dans l'URL**
* POST:
    * Body avec info, pas de query params
    * Jamais cached pas le browser, ni historique, impossible de mettre en favoris
    * Body de longueur "illimitée" (parfois protégé - DDos)
* PUT:
    * Comme POST, mais en targettant une ressource précise (path param dans l'URL)
* DELETE:
    * Réponse de succès ne garantit pas la suppression instantannée

* PUT, DELETE: You know what it is
* TRACE: obtention d'une copie de la requête envoyée (lorsque modifiée par un serveur lors de l'envoi)
* OPTIONS: Retourne méthodes authorisées par le serveur
* CONNECT: Conversion d'un requête en tunnel TCP/IP
* PATCH: Modification partielle d'une ressource

## URL

* Uniform Resource Locator
* String qui fait référence à la manière d'accéder à une ressource

## Headers HTTP

* Voir notes p.20-27

## Statuts HTTP

* Voir notes p.28-33

## HTML

* HyperText Markup Language
* Versions: 1.0 (1990), 2.0 (1995), 3.2 (1997), 4.0 (1999), 5 (2014)
* Extensions .html ou .htm

## CSS

* Cascading Style Sheet
* Définition de la représentation du HTML
* Usage possible pour afficher un XML ou un SVG ou tout autre format balisé (markup)

## Services Web

* API: Application Programming Interface
* REST, SOAP, BEEP, Hessian, JSON-RPC, etc.

### REST

* Representational State Transfer:
* Client-Serveur
* App Stateless
* Caching possible
* Interface uniforme:
    * Ressources identifiées de manière unique
    * Manipulation des ressources et des informations contenues
    * Le client ne présume pas (utilisation straight-forward des infos de l'API)
* Utilisation des verbes HTTP (GET, POST, PUT, DELETE)

* Avantages:
    * Facile d'entretien
    * Indépendance client-serveur, facilité de distribution
    * Caching facile
* Inconvénients:
    * Obligation de posséder toutes les infos nécessaires côté client
    * Stateless
    * Concept de session rend contraintes REST difficiles à respecter

### SOAP

* Simple Object Access Protocol
* Basé sur le XML
* Communication HTTP ou SMTP (plus rare)
* Constitution du message:
    1. Enveloppe: Identification du XML comme une requête SOAP
    2. Headers
    3. Body
    4. Fault: Contient les erreurs survenues durant la transaction

* Avantages:
    * Plusieurs protocoles de transport supportés (STMP, mais personne l'utilise)
    * Indépendant du langage et de la plateforme (so is REST you dumbass)
    * Évite problèmes de firewall rencontrés par de plus vieux protocoles
    * Signatures dans un WSDL (pas certains que c'est un adantage...)
* Inconvénients
    * Requêtes très lourdes en raison du format
    * Dépendance entre le client et le serveur

### XML

* Exensible Markup Language
* Permet à l'info d'être transmise et traîtée comme le HTML
* Beaucoup d'apps l'utilisent:
    * RSS feed
    * Documents Office (en XML en dessous)

### JSON

* JavaScript Object Notation
* Structure plus légère que le XML
* []: liste, {}: objet

## Requêtes Web sur Android

* HttpClient:
    * Deprecated depuis API 22
    * Pas de gestion async
* URLConnection:
    * Nouvelle méthode suggérée
    * Présente depuis API 1
    * Pas de gestion async

### HttpClient Async

* AsyncTask: Objet permettant de faire une tâche async
* HttpGet, HttpPost: Objet représentant les verbes HTTP

### Librairies réseau

* Voir diverses libs réseau p.65-67

## Cloud

* Cloud computing (infonuagique):
    * AWS
    * Google App Engine (GAE)
    * Azure
    * Digital Ocean
* Cloud computing Open Source:
    * AppScale
    * Eucalyptus

### IaaS

* Infrastructure as a Service
* Infra matérielle/virtuelle:
    * Firewall, LB, config IP, virtualisation
* AWS EC2, Window Azure, Rackspace Cloud

### PaaS

* Platform as a Service
* Plateforme de développement:
    * Gestion OS, DB
    * Apportez votre code, on s'occupe du reste
* Heroku, Google App Engine, AWS (certaines instances)

### SaaS

* Software as a Service
* Service/app géré par son fournisseur:
    * Data géré par le fournisseur
    * Service "on-demand"
* Salesforce, Google Apps, Office 365

### mBaaS / BaaS

* Mobile Backend as a Service
* Service de gestion de plateforme conçu pour apps mobiles
    * Intégration push avec FB/Twitter
    * Cloud storage
* Appcelerator Platform, Firebase

### *aaS

* Avantages:
    * Rapidité
    * Facilité d'accès
    * Facilité de déploiement
    * Uptime
    * Scalability
    * **Coûts**
* Inconvénients:
    * Vendor lock-in
    * **Coûts**
