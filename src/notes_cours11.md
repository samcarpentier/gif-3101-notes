# Capteurs et notifications (12 avril)

## Push notifications

* Pas vraiment du push:
    * IPs dynamiques, latence, firewalls
* Pull en continu pour recevoir les notifications en "temps réel"

### Push

* Affectif quand en background ou off
* Instantané
* Complexe de gérer l'énergie

### Pull

* Facile à implémenter
* Gestion d'énergie facile
* Plus lent
* Oblige app en fonction

### Firebase Cloud Messaging

* Permet l'envoi de notifications à in apareil (Android, iOS et web)
* **Gratuit** et sécurisé
* Possibilité d'envoyer à plusieurs devices
* Payload en JSON
* Ancienne version = payload 100% custom
* Nouvelle version = plus rigide, mais plus puissante
* Bidirectionnel

### Apple Push Notification Service

* Envoi de push à un mac ou un device iOS
* Utilise le token d'encryption du device + SSL/TLS (obligatoire)
* **Gratuit** et sécurisé (cert requis)
* Unidirectionnel

## Exploitation des capteurs

* GPS, micro, accéléromètre, caméra, NFC, bluetooth, jack audio
* Android = beaucoup plus de possibilités que iOS

### Localisation

* Latitude: [-90 (S), 90 (N)] degrés
* Longitude: [-180 (O), 180 (E)] degrés

#### GPS

* 31 satellites en orbite
* Composante spatiale: Au moins 4 nécessaires pour position valide
* Composante au sol: centre de gestion + 4 antennes
* Composante utilisateur: cellulaire

#### A-GPS

* Assisted GPS
* Signal satellite = 50 bits/s = 30-40 sec pour recevoir le premier signal GPS
* On souhaite assélérer le positionnement!
* Implantation de serveurs AGPS qui contiennent les infos GPS à jour: download plus rapide

#### Sur device mobile

* GPS consomme beaucoup d'énergie
* Réduire précision
* Cacher la dernière donnée de positionnement
* Android:
    * `ACCESS_COARSE_LOCATION`
    * `ACCESS_FINE_LOCATION`
    * Location manager: on s'y connecte pour avoir la position (service dormant)

### Caméra

1. Init la caméra manuellement et display dans une view:
    * Plus flexible
    * Plus complexe à implémenter
2. Init automatiquement la caméra et recevoir la réponse seulement (photo)
    * Plus facile à utiliser
    * Moins flexible

#### Android

* Chargement automatique:
    * Utilisation d'un intent via galerie photo
    * Utilisation d'un intent via l'appareil photo

### Accéléromètre

* Gyroscope:
    * Mesure vitesse angulaire
    * 0 si aucune rotation
* Accéléromètre
    * Accélération linéaire sur les 3 axes
    * Valeur en _g_: $1g = 9.8 m/s^2$
* Données combinées:
    * Tangage/Pitch (queue avion haut-bas)
    * Roulis/Roll (ailes de l'avion haut-bas)
    * Lacet/Yaw (queue de l'avion gauche-droite)

* Mise à jour:
    * Apple: 10-100 Hz
    * Android: différent d'un appareil à l'autre (généralement 4-200 Hz)

## Tests unitaires et tests de UI

### Tests unitaires

#### Local tests:

* Tester objets sans dépendance au framework Android
* Roulent sur la JVM locale
* Rapides et facile à rouler dans un CI engine
* Ne reproduisent pas l'environnement dans lequel l'app va rouler

#### Instrumented tests:

* Tests unitaires exécutés sur Android
* Exécution sur un device ou un simulateur
* Roulent dans l'environnement Android (pas de mocks des objets Android)
* Plus lents et requièrent absolument un device

#### Tests de UI

* Testent l'intégration avec le UI
* Simulation des interactions (tap bouton, etc.)
* Plus complexes à coder et plus flaky
* Exécution sur un device ou un simulateur
* Lib recommandée: `Espresso`
* Possibilité d'exécuter ces tests dans le cloud (Cloud Test Lab)

## Licensing et OSS

* Open source software:
    * Logiciel régit par une licence
    * Autorisation de redistribution, d'accès au code et de création de dérivés
* Free software:
    * Licence de logiciel qui garantit aux users que toutes le MaJ seront gratuites
    * Inclut l'accès au code source
* Différence entre les deux:
    * Free software oblige que les dérivés créés soient aussi Free Software
