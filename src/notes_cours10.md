# Sécurisation de données (5 avril)

## Préférences

* Accès rapide
* Usernames
* (mots de passe)
* Non-encrypté
* **Peu sécuritaire**
* Paires k-v
* Modes:
    * MODE_PRIVATE: seulement app en cours
    * MODE_WORLD_READABLE: lu par toutes les apps (deprecated depuis API 17)
    * MODE_WORLD_WRITEABLE: écrit et lu par toutes les apps (deprecated depuis API 17)

```java
// Write preference
SharedPreferences.Editor editor = getSharedPreferences("TAG", MODE_PRIVATE).edit();
editor.putString("name", "Mathieu");
editor.commit();

// Read preference
SharedPreferences prefs = getSharedPreferences("TAG", MODE_PRIVATE);
String name = prefs.getString("name", null);
```

## Keychain

* Aucune encryption matérielle garantie
* Utilisé seulement pour storer des clés et des certs
* Pas d'encryption de username/password avant API 18 (ajout encryption basée sur le matériel)

### 2FA

* Méthode d'auth qui utilise 2 composantes différentes
* Carte guichet + NIP
* Slack Bell (password + autorisation par cellulaire)
* VPN de Bell (TOTP (time-based one-time password))
* TOTP providers: Google authenticator, 1password, authy, freeotp, troopher

### Caching d'un password

* Non sécuritaire: SharedPreferences, SQLite, etc.
* Sécuritaire: NDK pour conserver la clé, OAuth, SmartLock, Crypto asymtrique

### Keychain Android

* Ne pas mettre clé dans le code (encryption password)
* Ne pas baser la clé sur des éléments spécifiques du téléphone (facilement retraçable)

### Keychain iOS

* Encryption 128-bits
* Storage de: passwords WIFI, compte courriel, certs VPN, Find My Phone, iCloud, clé d'encryption iMessage, SIM PIN

## OAuth

* Standard libre pour AuthZ
* Pas un protocole d'AuthN
* Versions 1.0, 1.0a, 1.1, 2.0, xAuth

### AuthN vs AuthZ

* AuthN: Validation de l'identité (login, validation de l'identité de l'utilisateur)
* AuthZ: Validation des permissions (accès aux ressources du système)

### OAuth 1.0 vs 2.0

* 1.0
    * Tokens longue durée (~1 an)
    * HTTPS pas requis
* 2.0
    * Ne requiert plus une page web
    * Tokens courte durée (~3 jours)
    * Pas de signature complexe
    * Pas de libs de crypto
    * HTTPS requis

### xAuth

* Pas de page web, flow plus adapté mobile
* Utiliser uniquement app "sécuritaire", sinon possibilité de se faire sniffer notre username/password

### Multitâches

* Synonyme: Multiplexage temporel
* Impression que l'on fait plusieurs tâches en même temps
* Différence PC/Mobile:
    * Mobile: Si l'app n'est pas visible, aucun code ne peut être exécuté (dépend de l'OS)
    * PC: Exécution de tâches par une app dès qu'elle est loadée en mémoire

### Processus

* Mémoire (et adressage) indépendate des autres processus
* Pas d'interaction sauf par les méthodes _interprocessus_ fournies par l'OS

#### Processus Android

* Dans le manifeste, les éléments _activity_, _service_ et _receiver_ peuvent prendre un tag `android:process`
* Peut être terminé à tout moment pas l'OS
* Possibilité d'avoir un même process pour plusieurs apps si elles **sont signées par le même cert**

#### _Thread-safety_ en Java

* Map: SynchronizedMap
* List: SynchronizedList
* Collection: SynchronizedCollection
* ArrayList: CopyOnWriteArrayList (recopié à chaque modification)
* `synchronized`: bloc de code qui barre l'accès à un objet jusqu'à la fin de l'exécution du bloc

```java
synchronized(something) {
    // Do thread-safe stuff
}
```

#### _Thread-safety_ Android

* Objet `Lock`:
    * ReentrantLock:
        * lock le thread et attends d'avoir le droit sur un objet
        * Similaire à `synchronized`, sauf qu'on choisir quand unlock
    * Condition:
        * Suspendre l'exécution d'un thread tant qu'une condition n'est pas remplie
        * Condition peut être remplie par un autre thread
    * ReadWriteLock:
        * Plusieurs lock de lecture et un seul d'écriture
        * Lock exclusif pour l'écriture
        * Notification des locks de lecture une fois écriture terminée

## Gestion d'énergie

* Schéma qui s'apparente à un ordinateur complet
* Baseband processor: gère communications radio (BT, WIFI)
* PMIC: Power Mgmt Integrated Circuit

### Importance en mobile

* Faire les bons choix de design
    * Calcul de l'opération sur le téléphone ou sur un serveur?
    * Effectuer cette opération de 2 minutes sur le téléphone ou non?
* Ne pas consommer de ressources inutilement (RAM, énergie)
* **No early optimization**; faut commencer quelque part, on améliorera après

## Regex

* **Voir table Regex dans les notes, p.61-63**
* **Voir exemples Regex dans les notes, p.64-68**
