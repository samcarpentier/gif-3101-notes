# Cours 1 (18 janvier 2018)

## TPs et projet

* TP1: dispo max 25 janvier
* TP2: dispo max 15 février
* TP3: dispo max 22 mars
* Mini-projet: Dispo max 15 février

## Plateforme

* Android SDK (Java et Open Source)

## Outils obligatoires

* Eclipse (non-obligatoire)
* Android Studio
* Android SDK
* Mercurial :(

* TP peuvent utiliser simulateur
* Utiliser vrai phone recommandé

## Introduction à l'informatique mobile

* Traffic internet mobile a dépassé le traffic "desktop" en 2017
* Omniprésent (voiture, épicerie, électroménagers, etc.)
* Élément déclencheur:
    * Palm OS (1996)
    * Premier Blackberry (2003)
    * Premier iPhone (2007)

* Apple vs Androis en 2007:
    * R&D Apple dépasse Android (premier Nexus)
    * Clavier physique vs touch screen d'Apple

* Apps > Matériel
* Corrélation apps et succès plateforme

* Contraintes:
    * Plus grosse: énergie (charge à tous les soirs)
    * Interruptions (SMS, appel, etc.)

## Objective C (dev iOS)

* Utilisé dans OS X et iOS
* Un des 2 langages (swift) pour faire du dev pour iOS, watchOS, tvOS et OS X

* Ressemble à ça C/C++
* Pas de method call, on fait du message passing
* Possibilité cass à "rien"
* GC à partir de ObjC 2.0

### Le code

* Notion de private n'est pas réellement privé, même avec annotation
* Nil signifie un pointeur qui n'a pas été instancié/alloué

* `if (self=[super init])` ne retourne jamais Nil (appelle ctor de base de NSObject)
* Méthode déclarée avec un + -> Accessible via appel statique (méthode de classe)
* Méthode déclarée avec un - -> Objet doit être instanciée (méthode d'instance)

* Appeler un getter/setter:

```objectivec
        Voiture *maVoiture = [[Voiture alloc] init];
        [maVoiture setNumberOfWheels:4];
        int wheels = [maVoiture numberOfWheels];
```

* Logging / Console output:

```objectivec
        NSLog(@"Number of wheels %d", wheels);
```

* Équivalents:

```objectivec
        int wheels = [maVoiture numberOfWheels];
        int wheels = maVoiture.numberOfWheels;
```

* Reste du message passing et non un appel de méthode
* Sera traduit en version avec [] par le compilateur
* Sert uniquement à faciliter la lecture
* Exemple plus complexe de la vie de tous les jours:

```objectivec
        [[[[NSArray objectAtIndex:1] getParent] objectAtIndex:2] numberOfWheels]
```

* Blocks: Segments de codes/fonctions utilisés pour des callbacks

```objectivec
        void (^simpleBlock)(void) = ^{
            NSLog(@"Ce bloc a été exécuté");
        };

        simpleBlock();
```

* Outil recommendé pour communications réseau en ObjC: AFNetworking
