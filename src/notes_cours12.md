# Mise en marché (19 avril)

* Android domine le marché pour le nombre de downloads (135% plus)
* Apple fait quand même le plus de profit...

## Développeurs

### iOS

* 120$ / année pour un compte dev chez Apple
* Droit de distribution (iTunes Connect)
* Version Beta des softwares Apple
* iTunes Connect: moyen pour afficher les apps sur iTunes

### Android

* 25$ de frais d'inscription uniques
* Distribution d'apps gratuites ET payantes
* Nécessite un compte Google + Google Wallet

## Publicité et revenus

### Sources de revenus

* Ad networks (airpush, admob, inmobi)
* Compagnies de placement (Atedra, Astral Media)
* Vente de produits (in-app purchase, achat intégré par CC)

### iAB (Interactive Advertising Bureau)

* Regroupement de compagnies qui font du placement de pub
* Définition et maintient des standards
* Établir ce qui fonctionne ou pas

### In-app purchase

* Frais de 30%, 70% au dev
* Si achat ajoute des features, utiliser l'achat in-app
* Si services externes à l'app, utiliser l'achat traditionnel
* Norme PCI DSS nécessaire: peut coûter jusqu'à 100K annuellement

### Science des changements comportementaux

#### Modèle de Fogg

* Pour que qqn effectue un changement, combinaison de 3 facteurs requise:
    1. La personne doit vouloir le faire
    2. La personne doit pouvoir le faire
    3. La personne doit être invitée à le faire
* Élément déclencheur efficace ssi personne très motivée (ou tâche facile)

* Exemple app FibeTV:
    * À la fin d'un épisode, on enchaîne le suivant
    * Motivation à voir la suite
    * Effort = nul

* En résumé: **Put hot triggers in the path of motivated people**

#### Principe de récompenses

* Boîte de Skinner:
    * Rat tire un levier = bouffe
    * Le rat s'habitue et tire le levier seulement quand il a faim
    * En variant les récompenses, le rat tire le levier plus souvent
* Parallèle avec les slot machines dans les casinos
* Dans les apps mobiles:
    * On "pull to refresh" pour voir combien de _likes_ notre nouvelle photo a
    * Swiper à droite = potentiellement la _femme de notre vie_ (yeah right)

## Reverse engineering

* Processus analytique pour découvrir la structure et les opérations sous-jacentes d'une software
* Motivations:
    * Comprendre fonctionnement
    * Copier un logiciel
    * Analyser produit fait par un concurrent
    * Hacking

* **DISCLAIMER: Following tools might or might not be legal to use**:
    * Androguard, smali, dexdump, apktool, dex2jar, Java Decompiler, UI Automater

## Futur de la mobilité

* Wearable tech:
    * ~36M d'appareils
    * Augmentation de la popularité
