# Cours 6 (22 février)

## Design d'interfaces

* Processus decréation: représenté par les sous-titres suivants

### Définir les buts

#### Buts de l'utilisateur

* Dicerner ce qui est important
* **Ne pas mélanger tâches et buts**:
    * Tâche: Pouvoir lire les tweets du fil d'actualité
    * But: Pouvoir lire les tweets que je trouve pertinents
* Découpage si nécessaire

* Exemple: App de photos
    * Je veux pouvoir manipuler facilement mes photos (but)
        * Visualiser toutes les photos sur mon téléphone (sous-but)
        * Améliorer la qualité visuelle des photos (sous-but)
        * Partager mes photos (sous-but)

#### Types d'utilisateurs

* Considérer différents types d'utilisateurs:
    * Photographe pro avec gros budget
    * Gars qui veut juste poster ses photos sur FB

#### Buts du produit

* Faire des choix:
    * Type users visés plus restreints
    * Limiter fonctionnalités réellement intégrées dans l'app
* Fixer buts:
    * Dispo sur Play Store dans 15 semaines
    * Faire 2000$ en 3 mois
* Intersection entre NOS buts et les buts des UTILISATEURS

#### Bornes du support

* Choisir version du SDK plus tôt que tard
* Cibler features critiques et support dans SDK
* Phase de tests

### Établir les interactions de haut niveau

* Trouver chemins menant aux différents buts
* Prioriser les fonctionnalités
* Pas liste exhaustive de toutes les possibilités de navigation
* Forme de schéma-bloc
* (Étape négligée par les débutants)

### Développement de maquettes (wireframes)

* Élaboration UI/UX en même temps (beau ET fonctionnel)
* Une des dernières étapes avant la production
* Tester idées, valider hypothèses UX

* Croquis des interfaces (manuscrits ou informatisés)
* Schémas de navigation dans l'app pour accomplir les buts des users
* Utilisation d'outils de prototypage (JustInMind/Pencil = freeware de maquettage)

#### KISS (Keep It Simple, Stupid)

* Single responsibility

### UI et UX

* UI: à quoi l'interface ressemble ou non
    * Pas texte rose sur fond mauve
* UX: à quoi l'expérience ressemble ou non
    * Utilisation d'un NavigationDrawer au lieu d'un TabBar
* Possibilité d'avoir un bon UX mais un UI de merde et vice-versa

#### Point focal

* Attirer l'oeil du user sur LE point important de notre interface (élément qui se distingue)
    * Forme, grosseur, couleur, animation
    * Bouton commander sur une page d'achats en ligne

#### Concepts de base du UX en mobilité

* Assomption: Le user EST distrait
* **Ne pas afficher l'information pertinente de manière temporaire** (toast): utiliser un dialog (requiert une action du user)
* Hiérarchie du contenu:
    * Généralement: design Flat = mauvais UX (trop chargé)
* Lisibilité et contenu:
    * Maximiser le contenu (c'est la raison de l'existence de l'app)
    * Interligne/marges respectables
    * Bien espacer les éléments
    * Exemple: chrome -> barre d'adresse réduite quand on scroll
    * Textes et éléments avec bon **contraste** (voir norme W3C)

##### Adaptation à l'utilisation tactile

* Atteignabilité:
    * Zones importantes plus proches des doigts (selon position dans laquelle on tient l'appareil: statistiques)
    * Pinky boundary: le user se sert de son "petit doigt" pour stabiliser les gros appareils (résuit la portée du pouce)
* Zones actives:
    * Assez grandes et espacées
* Affordance:
    * Capacité d'un objet à avoir le comportement attendu (porte push/pull selon le type de poignées)
    * Ajout de texte pour renforcer l'afforfance
    * Combinaison avec le point focal

##### Menus et contextes

* Idéal: Toujours avoir le menu visible
* Réalité: pas d'espace pour ça
* Avantage: toujours disponible en un clic (tap), sans changer le contexte courant
* Créer une hiérarchie cohérente avec le contenu de l'app

##### Faciliter la navigation

* Éviter la navigation: économier des taps au user
* Faciliter navigation transversale (onglets)
* Réduire le nombre de taps pour faire les actions importantes
* **Faire exprès de couper l'écran pour exposer la possibilité de _scroller_**
* Pinner un menu ou une navbar en haut

#### Concepts UI de base en mobilité

* UI de l'app influencé par le UI de l'OS

* Skeuomorphisme: ressemblance avec un objet réel (boussole, niveau)
    * Force des interfaces plus lourdes (textures, etc), mais plus réelles
* Flat design: opposé du skeuomorphisme: on place tout à plat (minimaliste) pour éviter les textures et simplifier au maximum

##### Couleurs

* Cercle chromatique:
    * Couleurs primaires (rouge, bleu, jaune)
    * Couleurs secondaires (à mi-chemin entre les primaires)
    * Couleurs tertiaires (à mi-chemin entre les secondaires)

* Ombres: couleur + noir
* Nuances: couleur + blanc
* Monochrome: Même couleur d'origine, variation de blanc et de noir ajouté
* Analogues: couleurs adjacentes sur le cercle chromatique
* Complélentaires: couleurs diamétralement opposées sur le cercle chromatique
* Complémentaires adjacents: couleurs adjacentes à la couleur analogue

* Couleurs informatiques:
    * RGB: 
        * 0@255
        * Possibilité de représentation hexa
    * CMYK (cyan, magenta, yellow, black)
    * HSB (hue, saturation, brightness)
    * Lab (lightness)
    * Pantone:
        * Couleurs standardisées pour l'impression
        * 2058 couleurs

##### Lignes directrices Android (UI)

* Appelées à changer avec le temps (jugement critique avant de tout appliquer à la lettre!)

* Ajouter animation subtile/surface avec beau design
* Utiliser des objets réels
* Laisser place à la personnalisation
* Donner des rétroactions au user
* Être poli et clair dans les messages d'erreurs
* Afficher limite des interfaces (griser boutons, etc)
* Éviter contrôles trop petits (écran tactile)
* Police d'écriture et couleurs (utiliser Android stuff)
* [Voir les autres recommandations](https://developer.android.com/design/get-started/principles.html)

#### Styles et thèmes

* Propriétés qui décrivent le format et l'apparence des vues
* Contenues dans `res/styles`
* Comparable au CSS des pages web
* Support héritage (facile de modifier certains éléments pour l'adapter à nos goûts/besoins)
