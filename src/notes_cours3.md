# Cours 3 (1er février)

## Architecture d'une app Android

### Fichiers générés, outils et leur utilité

* Le fichier R.java contient tous les IDs de chaque composante graphiques pour pouvoir les retrouver dans le code
* Le fichier AndroidManifest.xml décrit les composantes et les paramètres de l'app
* Le fichier local.properties contient les configs pour la compilation de l'app
* Le fichier settings.properties
* Utilisation de Gradle built-in dans Android Studio pour gérer les dépendances

* Outil _Proguard_:
    * Optimise et protège les apps lors des releases
    * Retire classes non utilisées
    * Optimise le bytecode
    * Obfuscate le nom des classes
    * Retire logging pour les releases

### Styles architecturaux

* MVC:
    * Model: modèle de données
    * View: vue visible par l'utilisateur
    * Controller: Logique de contrôle, évènement, "glue" entre model et view
    * Graphe acyclique (view -> ctrl, ctrl -> model, model -> view)
    * Controllers partageables à travers les views, car basés sur actions

* MVP:
    * Comme MVC, mais flow différent avec le Presenter
    * Pas d'interaction entre model et view
    * Tout passe par le presenter
    * Gestion évènementielle onmiprésente
    * Normalement un seul presenter par view (plusieurs presenters si view complexe)

* MVVM:
    * Comme MVP, mais flow différent avec VM
    * View a des bindings sur le VM et des évènements sont triggered quand le modèle change pour updater la view

* PAC:
    * Presentation: Formate et présente l'info
    * Abstraction: récupère et traite l'info
    * Control: Communication entre A et P, gère le flow d'info

* HMVC:
    * Hiérarchie de MVC
    * Plusieurs patterns MVC les uns en dessous des autres (arbre)
    * Apps plus complexes

* Dans android:
    * Pas clair si MVC ou MVP, tend plus vers MVP
    * Object = model
    * View = view (layout, etc)
    * Activity = controller/presenter

* Dans iOS:
    * MVC et HMVC
    * NSObject = model
    * UIView = view
    * UIViewController = controller

## Cycle de vie d'une Activity

### Les trois boucles

* Boucle de vie
    * Contient les boucles de vie visible et non visible
    * Dans cette boucle, activity chargée en mémoire

* Boucle de vie disponible
    * Signifie que l'activity est disponible
    * Aucune garantie qu'elle a le focus/qu'elle est en premier plan
    * Exemple: autre app ouverte durant appel téléphonique

* Boucle de vie visible premier plan
    * Quand l'utilisaeur utilise l'app

* Activities gérées selon LIFO (généralement)

### Intent

* Opération à effectuer
    * Démarrer activity
    * Démarrer/s'enregistrer à un service
    * Diffuser message

* Démarrer une nouvelle activity:

```java
      Intent myIntent = new Intent(getApplicationContext(), MyActivity.class);
      startActivity(myIntent);
```

* Démarrer nouvelle activity avec options:

```java
      Intent myIntent = new Intent(getApplicationContext(), MyActivity.class);
      myIntent.putExtra("MODEL", "CLA-2014");
      startActivity(myIntent);
```

### Interfaces, delegates et services

* Interface
    * Échanger des messages et de l'info entre plusieurs systèmes
    * Utilisé pour réponses async
    * Recevoir messages inter-activité

* Interfaces en Java
    * Tu sais c'est quoi...

* Delegate (iOS) ou `Protocol`
    * Passer l'info du modèle (MVC) en respectant les principes OO

* Service
    * Tâche qui n'a pas besoin d'interagir avec un user
    * Roule généralement en background
    * Pas nécessairement un process séparé
    * Pas un thread
    * Deprecated depuis Android 8+
    * `JobScheduler` est utilisé maintenant

* Opérations et états d'un service:
    * Démarré:
        * Appel à `startService()`
        * Fonctionnel jusqu'à l'appel de `stopService()` ou `stopSelf()`
    * Lié:
        * Clients appellent `bindService()` (plusieurs clients peuvent utiliser le même serivce)
        * Service sera détruit quand tous les clients auront appelé `unbindService()`
    * Possibilité de se lier à un service démarré

## Communication intra-application

### `BroadcastReceiver`

* `Broadcast`
    * Permet de transmettre des infos intra/inter-application
    * Ouvert à toutes les apps
    * Possibilité de restreindre depuis Android 4.0 avec `Intent::setPackage`
* `LocalBroadcastManager`
    * Broadcast mais local au processus qui l'envoie
    * Ne sort pas de l'app en cours
    * Plus efficace qu'un `Broadcast` normal

* Envoyer un broadcast (100% async): `Context.sendBroadCast(...);`
    * Impossible d'arrêter l'envoi à tous
* Envoyer un broadcast ordonné (ordre priorité): `Context.sendOrderedBroadcast(...);`
    * Possible d'arrêter l'envoi

* Alternatives au broadcasting
    * `PropertyChangeListener`/`Observer`: Permette de subscribe pour être informé d'un changemet sur un objet
