# Applications Web (29 mars)

* Types d'applications
    * Native: Exploite fonctions de l'OS sans intermédiaire. Installée localement.
    * Multi-plateforme: Exploite indirectement fonctions de l'OS. Couche d'abstraction logicielle.
    * Hybride: À la fois native et web
    * Web: Exploitable via un browsr, exploite techno web (CSS, JS, HTML)

## L'application web

* Avantages
    * Compatibilité multiplateforme (iOS/Android/BB/Windows phone)
    * Coûts généralement moindres
    * Développement rapide et facilité de MaJ/Bugfixes
* Inconvénients
    * Performance moindre
    * Limite la quantité de contenu (HTML5 ~40Mo)
    * Interfaces plus simplistes

### HTML5

* Cache d'application
* Geolocation, accéléromètre, gyroscope, etc.
* DB (Web SQL, IndexedDB)
* Animations CSS3
* **Non-standardisé**

## L'application native

* Avantages
    * Plus performant
    * Utiliser toutes les features du device
    * Complexité des interfaces
    * Quantité de data ++
* Inconvénients
    * Coûts
    * Durée du développement
    * Processus de MaJ/Bugfix
    * Dév par plateforme

## L'application hybride

* Activation et liens entre features natives et interface web
* Utilisation de push notifications
* Facebook avant, maintenant native

## L'application multi-plateforme

* Northstar des devs (désir de faire fonctionner une seule app partout)
* _Develop once, run everywhere_ ~ Sun Microsystems about Java
* Réalité java: _Develop once, debug everywhere_

### Outils pour du multi-plateforme

* Appcelerator Titanium
    * Business logic (model + ctrl) réutilisable
    * Reste (view + ctrl) par plateforme
* PhoneGap
    * Dév de modules natifs rendus accessibles au JS
    * Nécessite un WebView, donc on se retrouve avec les problèmes des apps web
* Corona SDK
    * Code en Lua
    * Payant (1k/an/dev)
* Xamarin
    * Permet d'utiliser C#
    * 1k/an/entreprise

## Bases de données

### Approche par fichiers

* Repo in-memory sauvegarde le data dans un fichier
* Différent fichier de data pour chaque repo (type de sauvegarde)
* Perte d'espace, duplication de data, format différent
* Structure de données définie dans le code
* Difficile de partager le data entre processus (concurrence)

### Approche par SGBD (DBMS)

* Un système de gestion de BD s'occupe de persister les données
* Les sources de données push le contenu dans le SGBD

* DDL: Data Definition Language (`create table`, `alter table`)
* DML: Data Manipulation Language (`insert`, `update`, `delete`)

* Avantages
    * Accès concurrent géré
    * Contrôle redondance, cohérence data, partage
    * Sécurité
    * Standardisation
    * Backup et restauration
* Inconvénients
    * Complexe
    * Coût
    * Performance
    * Bottleneck en cas de panne

#### Timeline

| Époque | Technique | Technologie |
| :---: | :---: | :---: |
| 1960 | Systèmes par fichiers ||
| 1970 - première gébération | Hiérarchie, réseau ||
| 1980 - seconde génération | Relationnel | SQLite, MySQL |
| 1990 - troisième génération | Objets relationnels, orienté objet | PostgreSQL, Oracle |
| 2010 - quatrième génération | NoSQL, post-relationnel, orienté document | MongoDB, ElasticSearch, CouchDB |

### SQLite dans Android

* `android.database`: Accès à un content provider (couche supérieure de la DB, aucun détail d'implémentation)
* `android.database.sqlite`: Donne accès à la lib de gestion de DB SQLite 3.4.0
* **Voir exemple de code Java notes p.56-59**

### Libs ORM

* ORM: Object-relational mapping
* Expose une interface de DB OO à partir d'une DB relationnelle

* OrmLite (encore supporté)
* GreenDao (un peu comme Kafka, event bus, subscribe a des topics)
