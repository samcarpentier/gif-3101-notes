# Cours 4 (8 février)

## Permissions

* Grande quantité d'information (device qu'on a toujours sur soi)
* Localisation GPS
* Contacts
* Photos
* Obligation d'informer l'utilisateur des droits qu'il confère à une app

* APIs < 23:
    * Permissions à l'installation de l'app
    * Aucun moyen de révoquer
    * Confiance aveugle au dev

* APIs >= 23:
    * Demandées au moment d'utiliser un service (photo, contacts, etc.)
    * Possibilité de révoquer en tout temps
    * Dev peut expliquer pourquoi il veut telle ou telle permission

### Permissions Android

* Définies dans le Manifest

```xml
      <uses-permission android:name="android.permission.INTERNET">
```

* Les plus utilisées:
    * `android.permission.INTERNET`
    * `android.permission.CAMERA`
    * `android.permission.ACCESS_NETWORK_STATE`
    * `android.permission.ACCESS_FINE_LOCATION`
    * Liste exhaustive dans PDF page 8-10 ou [ici](https://developer.android.com/reference/android/Manifest.permission.html)

### Manifeste Android

* Décrit l'environnement dans lequel l'app roule
* Nom, icône, SDK minimum, version, services, permissions, etc.
* **Void PDF pour outline d'un manifeste plus complet**

### Permissions iOS

* Définies dans le fichier `Info.plist`
* Expressions clé-valeur propriétaire Apple (XML déguisé)
* Nom app, nom bundle, icône, version, nom de l'exécutable, restrictions, fonts ajoutées manuellement

* Demandées au cas par cas
* Généralement demandées au moment du premier usage d'une feature demandant une permission donnée
* Acquises dans le code via des méthodes spécifiques
* Possibilité de révoquer à tout moment (user maître de ses données)

## Sécurité mobile

* Toujours sur moi = sécuritaire (**FAUX!**)
* Aucun besoin d'un anti-virus (**FAUX!**)
* En 2016, 18.4M de malwares mobiles détectés!

### Sécurité Android

* Applications roulent dans un sandbox
* Accès à un espace commun à toutes les apps
* Roulent sur le kernel Linux

* Android 4.0+:
    * Cert user-generated
    * Possibilité d'utiliser un keychain
* Avant 4.0:
    * Responsabilité du dev de bien encrypter ses données

* Normalement chaque app roule dans un process à part
* Avec les bonnes permissions, possibilité de faire rouler 2 apps dans un même process:
    * Même signature
    * Même ID user linux

* Encryption 128-bits dérivée du mot de passe (depuis Android 3.0)
* APIs open source
* Possibilité d'installer des apps de source inconnues
* Processus de MaJ lent

### Sécurité iOS

* Applications roulent dans un sandbox (plus strict qu'Android)
* Aucun espace commun à toutes les apps
* Toutes les apps sont signées avec un cert d'Apple
* L'utilisateur n'est pas _root_ sur son téléphone

* Bootloader, kernel et extensions du kernel signées par un cert propriétaire à Apple
* Virtuellement impossible de chager l'OS pour un autre non signé par Apple
* Validation des certs à chaque démarrage du téléphone
* Encryption 256-bits avec clé sur la mémoire interne de l'appareil
* Impossible d'installer des apps de sources inconnues
* Processus de MaJ rapide

* Mot de passe _root_ sur iOS: _alpine_

* ASLR (Address Space Layout Randomization):
    * Évite les attaques du type _stack overflow_
    * Stack, fonctions, etc placés aléatoirement dans la mémoire

* iMessage:
    * Encrypté E2E
    * Clé storée sur l'appareil
    * Encryption par l'appareil lui-même

## Philosophie d'affichage

* Beaucoup plus de tailles d'écrans à gérer avec Android qu'iOS

* iOS:
    * Pixel-perfect (photo JPEG, PNG, etc.)
* Android:
    * Image scriptée (SVG, CSS, etc.)
    * Sera sous forme de pixels une fois rendered

| | Pixel Perfect | Image scriptées |
| --- | --- | --- |
| Avantages | Plus de précision, moins de calcul | Supporte toutes les tailles d'écrans, relation entre les éléments |
| Inconvénients | Complexe si plusieurs formats à supporter | Plus de puissance de calcul nécessaire |

* SVG (Scalable Vector Graphics):
    * Supporté nativement par Android 4.4+ (API 20)
    * Avant API 20, Gradle génère dymaniquement les PNGs à la compilation
    * Va scaler automatiquement pour être toujours du même format peut importe la grandeur/résolution de l'écran

### Android Canvas

* Couche logique qui s'occupe de dessiner un objet
* 4 éléments nécessaires:
    * _bitmap_, contenir les pixels
    * _canvas_, dessiner des pixels sur un bitmap
    * Objet primitif (Rect, Path, test, Bitmap)
    * Objet _Paint_ qui décrit la couleur/gradient à appliquer
* Implémentation d'Android Canvas utilise **Skia**

* Android 3.0+, accélération GPU avec OpenGL
* API 14+, accélération matérielle activée par défaut

### Android Layers

* Voir tableau synthère dans les notes (page 64)
* Skia: engin graphique 2D utilisé en background par Android
* Affichage par couches:
    * Outils payants pour les inspecter sous iOS (Spark Inspector, RevealApp)
    * Outil inclus dans Android Studio gratuit (Hierarchy Viewer)
    * Permettent de débugger le UI
