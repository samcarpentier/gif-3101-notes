# Cours 2 (25 janvier 2018)

## Détails TP1

* Installer Android Studio
* Rapport PDF avec screenshots pour prouver que l'installation est fonctionnelle
* Application HelloWorld comme détaillé dans l'énoncé
* Pas toute la matière est dipo en date d'aujourd'hui
* *DOIT fonctionner sur l'API 15*

## Swift

* Plus performant que ObjC
* Développé par Apple pour:
    * iOS
    * macOS
    * tvOS
    * watchOS

### ABI: Application Binary Interface

* API entre 2 binaires compilés
* Non présent dans les langages non compilés: doivent se conformer
* Lien avec swift:
    * Anciennement, Swift 2.0 non compatible avec Swift 3.0
    * Version de 2018 devrait inclure le ABI Lock (version fixe de l'ABI)

### Le code

* Nil signifie aucune valeur, pas objet non-instancié
* Beaucoup d'exemples de code, voir slides ...
* Appel d'un setter en swift:

```swift
        func setNumberOfWheels(wheels:Int) {
          mWheels = wheels
        }

        voiture.setNumberOfWheels(wheels:4)
```

* L'opérateur `_` sert à caller un setter comme en Java:

```swift
        func setNumberOfWheels(_ wheels:Int) {
          mWheels = wheels
        }

        voiture.setNumberOfWheels(4)
```

* Struct en Swift:
    * Toujours passé par copie
    * Classe normale passée par référence

```swift
        struct Voiture {
          var wheels = 4

          init(){
          }

          func hasWheels() -> Bool {
            return true
          }
        }
```

* Extension:
    * Permet d'overrider et d'ajouter des fonctions à un type existant
    * Comme prototype en JS
    * Même sur les types natifs

* Closures:
  * Comme blocks en ObjC ou yield en python

* Cast quand on n'est pas certain (as?):

```swift
      if let bmw = voiture as? BMW {}
```

* Cast quand on est certain (as!):

```swift
      if let bmw = voiture as! BMW {}
```

## JAVA

* Je skip tout ça...
* Exemple de code et conventions

## Outils de développement

* SDK (Software dev kit)
* NDK (Native dev kit)
* DDMS (Sert de debugger)
* TP *DOIVENT* fonctionner sur l'API 15

## Android

### Fragmentation

* Fragmentation utilisation/versionnage
* Trop de gens utilisent des vieilles versions
* 2x plus de gens utilisent une vieille version de 2011 p/r à la plus récente
* Biggest pain: Il faut toujours être rétrocompatible

* Comparatif avec iOS:
    * en 96h, >50% des utilisateurs avaient migrés (iOS 7)
    * Après 4 mois, >85% (iOS 7,9,10)

* Pourquoi?
    * Processus MaJ Android est *très* complexe
    * Android essaie de faire plaisir à tout le monde
    * Open source (application MyBell non désinstallable quand on signe avec Bell)
    * Telco carriers & fabricants téléphones ajoutent du _bloatware_ pour tenter de se différencier de la compétition

* Chez iOS:
    * Aucun telco carrier n'a le droit de modifier l'OS, moins de délai

### Autres points de complexité

* Taille d'écrans très variables
* Grand nombre de marques
