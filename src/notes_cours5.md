# Cours 5 (15 février 2018)

## Outils et éléments graphiques

### Éléments graphiques Android (Widgets)

#### Activity

* Tâche spécifique pour un utilisateur
* Déclarée dans le manifeste
* Peut contenir (ou non) un fragment

#### Fragments (overview)

* Depuis API 11
* Split screen pour améliorer interactions
* Ce qui s'affiche sur 2 écrans sur un cell peut être dans un seul sur une tablette (même activity, fragment différent)
* Possède toujours une activité reliée
* **Support des différentes tailles écrans**

#### Layouts

* Relative: position des widgets p/r aux autres
* Linear: position verticale ou horizontale (widgets en ligne)
* Absolute: position absolue peu importe taille du device (API > 3)
* Frame: position avec donnée de gravité (découpage parties interface)
* Grid: organisation des widgets en cellules (pas confondre avec GridView)
* Constraint: comme relative, mais plus simple d'utilisation (conversion de l'un à l'autre possible)

#### Arrangement visuel (layout params)

* Chaque layout a ses propres params
* `FILL_PARENT`: Prendre autant de place que le parent (API > 8)
* `MATCH_PARENT`: Aussi gros que le parent (API <= 8)
* `WRAP_CONTENT`: Aussi gros que ses éléments

#### Views diverses

* Élément de base de tous les widgets dans Android

* ListView: Conteneur de _n_ views (cellules) présenté sous forme de liste (définies dans un Adapter)
* GridView: Conteneur de _n_x_n_ views (cellules) présenté sous forme de quadrillé (définies dans un Adapter)
* ScrollView: Permet déplacement dans l'écran quand le contenu est plus grand
* ImageView: Affichage d'une image
* RecyclerView: Affichage d'une grande quantité de data dans un espace réduit (plus avancé)

* Autres contrôles:
    * Base: Button, RadioButton, Switch (onn/off), ToggleButton, Spinner (liste déroulante), Picker (Date, Time, Number)
    * Texte: TextView (read only), TextEdit (read/write), DigitalClock/TextClock
    * ProgressBar, SeekBar (comme pour le volume)
    * WebView:
        * Afficher une page web embedded dans l'app
        * Recevoir code HTML et l'afficher
        * Puissant par sa nature web
        * Plus lent que browser natif
        * Possibilité de proxy et intercepter les requêtes/réponses
    * Dialogue:
        * AlertDialog: Focus au premier plan, requiert action du user pour se fermer
        * Date/TimePickerDialog: Idem
        * Toast: Petit pop-up informatif (tooltip)
            * `Toast.makeText(getApplicationContext(), "un message quelconque", Toast.LENGTH_SHORT);`
    * Recherche:
        * SearchView (API <= 11) add-on a une barre d'action, SearchDialog (géné par l'OS) dans la barre d'action du haut
        * MapFragment (API <= 12, avant SupportMapFragment) affiche carte avec position GPS du device, utilise GooglePlay Services

#### Menus contextuels

* Sert à ajouter du contenu sans bloater l'interface:
    * Touch & hold pour sélectionner (Gmail)
    * Touch & hold pour afficher plus d'options
    * Ajout d'une icône "..." pour afficher plus d'options
* Gestion overflow:
    * Menu key (built-in Android) - Hamburger dans le bas du screen
    * Icône "..." pour afficher le menu Android complet (ou swipe down/up)
* Floating action button:
    * Le gros signe "+" qui flotte dans Gmail

* Menu hamburger (Google Photo): s'expand dans l'écran quand on clique dessus
* Tab bar: Menu au top de l'écran pour naviguer (Bell Fibe sur TvOS)
    * Problématique quand trop d'éléments (difficile de s'y retrouver)
* BottomNavigationView: TabBar dans le bas de l'écran

#### Action bar

* Depuis API <= 11
* Facilite navigation entre les actions
* Calqué sur iOS

#### Support libs

* V4:
    * API 4+
    * Fragment, NotificationCompat, LocalBroadcastManager, ViewPager, ViewGroup
* V7:
    * API 7+
    * ActionBar
    * AppCompatActivity

### Fragments

* Prend vie dans le `Context` d'une Activity
* Possède son propre lifecycle
* Pas nécessairement un UI
* Possibilité d'**ajout** et de **suppression** dynamique -> puissant
* Dispo depuis API 11
    * Avant on parle de FragmentActivity
    * Si on utilise un Fragment de la lib de support, on parle de SupportFragment maintenant
    * Impossible de mélanger Fragment et SupportFragment

#### Cycle de vie

* `onStart()`: devient visible
* `onAttach()`: attache le fragment à une activité
* `onDetach()`: se détache de son activité parente
* Voir autres dans notes de cours (comme Activity)

#### Communication et méthodes pratiques

* Ne jamais faire communiquer fragments entre eux
* Ne jamais avoir conscience de son activité parente
* **Toute** communication devrait passer par activité hôte
    * Hôte implémente interface de communication
    * Validation faite par `onAttach()`

* FragmentManager: gère interactions entre fragments dans une Activity
* FragmentTransition: gère opérations sur un fragment (attach/detach/...)
    * Doit se terminer par un `.commit()` ou `.commitNow()`
* `Fragment.isInLayout()`

### Exemple d'utilisation des outils graphiques

* Voir démo dans les notes avec Android Studio
