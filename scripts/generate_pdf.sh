#!/bin/bash

markdown_source_files=(`find src/ -name '*.md' | sort --version-sort`)

mkdir -p target
pandoc --standalone config.md  ${markdown_source_files[*]} -o target/notes_gif_3101.pdf
